#-------------------------------------------------
#
# Project created by QtCreator 2014-03-21T17:01:56
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = StreamingServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    streamserver.cpp

HEADERS += \
    streamserver.h


INCLUDEPATH += $$PWD/liveMedia/include
DEPENDPATH += $$PWD/liveMedia/include
LIBS += -L$$PWD/liveMedia/ -lliveMedia

INCLUDEPATH += $$PWD/UsageEnvironment/include
DEPENDPATH += $$PWD/UsageEnvironment/include
LIBS += -L$$PWD/UsageEnvironment/ -lUsageEnvironment


INCLUDEPATH += $$PWD/groupsock/include
DEPENDPATH += $$PWD/groupsock/include
LIBS += -L$$PWD/groupsock/ -lgroupsock

INCLUDEPATH += $$PWD/BasicUsageEnvironment/include
DEPENDPATH += $$PWD/BasicUsageEnvironment/include
LIBS += -L$$PWD/BasicUsageEnvironment/ -lBasicUsageEnvironment
