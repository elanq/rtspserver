#include <BasicUsageEnvironment.hh>
#include "streamserver.h"

int main(int argc, char *argv[])
{
    TaskScheduler *scheduler = BasicTaskScheduler::createNew();
    UsageEnvironment *env = BasicUsageEnvironment::createNew(*scheduler);

    UserAuthenticationDatabase *authDB = NULL;

#ifdef ACCESS_CONTROL
    authDB = new UserAuthenticationDatabase;
    //replace with real string
    authDB->addUserRecord("username1","password1");
#endif

    //create RTSP instance with default port number 554
    //alternative port number is 8554
    RTSPServer *rtspServer;
    portNumBits rtspServerPortNum = 554;
    rtspServer = StreamServer::createNew(*env,rtspServerPortNum, authDB);

    if(rtspServer==NULL)
    {
        rtspServerPortNum = 8554;
        rtspServer = StreamServer::createNew(*env,rtspServerPortNum, authDB);
    }
    if(rtspServer==NULL)
    {
        *env << "Failed to create RTSP server : "<<env->getResultMsg() <<"\n";
    }

    *env << "play stream from this server using this URL \n";
    *env << rtspServer->rtspURLPrefix();
    return 0;
}
